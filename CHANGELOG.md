
# NDD Check4J

## Version 0.8.0

- feat: add Iterable checkers (close #6)
- fix: Sonar issues
- fix: code formatting
- feat: use [ndd-maven](https://gitlab.com/ndd-oss/java/ndd-maven) projects as parent
- fix: remove CheckStyle imports

## Version 0.7.0

- feat: add CI build
- feat: add checks for java.nio.file.Path
- feat: add checks for java.io.File
- refactor: all
- build: move from BitBucket to GitLab

## Version 0.6.1

- [QUALITY] Fix CheckStyle warnings on parameters order

## Version 0.6.0

- [DESIGN] Move package `name.didier.david.check4j` to `name.didier.david.check4j`
- [DEPENDENCY] Update plugins versions
- [DEPENDENCY] Update parent POM to use Java 8

## Version 0.5.3

- [DEPENDENCY] Updated the version of the `ndd-parent-java` dependency to `0.2.4`
- [QUALITY] Updated Checkstyle configuration to version 6.3

## Version 0.5.2

- [ENHANCEMENT] Fixed `FluentCollectionChecker` and `ConciseCollectionChecker` to remove the need to cast
- [QUALITY] Added PMD report, see [NDD-CHECK4J-2](https://bitbucket.org/ndd-java/ndd-check4j/issue/2/add-pmd-report)
- [QUALITY] Fixed warnings
- [QUALITY] Fixed formatting

## Version 0.5.1

- [DEPENDENCY] Updated the version of the `ndd-parent` dependency to `0.2.2`

## Version 0.5.0

- [BUILD] Created the new project `ndd-parent-java`
- [QUALITY] Added Checkstyle import control check

## Version 0.4.1

- [BUILD] Normalized TestNG suite files
- [FEATURE] Updated `ConciseCheckers#checkInstanceOf` to cast result

## Version 0.4.0

- [BUILD] Created the new project `ndd-parent`
- [QUALITY] Updated Checkstyle configuration

## Version 0.3.2

- [FEATURE] Added  `ConciseCheckers#checkInstanceOf`
- [FEATURE] Added  `FluentChecker#isInstanceOf`
- [QUALITY] Cleaned up and refactored unit tests

## Version 0.3.1

- [FEATURE] Added  `FluentCollectionChecker` and `ConciseCollectionChecker`
- [FEATURE] Added  `FluentObjectArrayChecker` and `ConciseObjectArrayChecker`
- [QUALITY] Cleaned up unit tests
- [QUALITY] Prefixed all fluent checkers with `Fluent`

## Version 0.3.0

- [BUILD] Added documentation to help the release process
- [FEATURE] Added number concise checkers:  `ConciseDoubleChecker`, `ConciseFloatChecker`, `ConciseIntegerChecker`, `ConciseLongChecker`
- [FEATURE] Added number fluent checkers:  `FluentDoubleChecker`, `FluentFloatChecker`, `FluentIntegerChecker`, `FluentLongChecker`

## Version 0.2.2

- [BUILD] Prepared to release to Sonatype OSS Maven Repository

## Version 0.1.0

- [DOCUMENTATION] Added the readme file

## Version 0.2.0

- [ENHANCEMENT] Renamed `Checkers` to `FluentCheckers`
- [FEATURE] Added `ConciseCheckers`
- [QUALITY] Updated Checkstyle naming rules for abstract test cases
- [QUALITY] Added JDepend report

## Version 0.1.0

- Initial commit
