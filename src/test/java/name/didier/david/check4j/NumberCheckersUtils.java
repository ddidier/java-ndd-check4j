package name.didier.david.check4j;

final class NumberCheckersUtils {

    static final String EXPECTED_LESS = "Expected parameter to be less than <%s> but was <%s>";
    static final String EXPECTED_P_LESS = "Expected parameter 'P' to be less than <%s> but was <%s>";
    static final String EXPECTED_STRICTLY_LESS = "Expected parameter to be strictly less than <%s> but was <%s>";
    static final String EXPECTED_P_STRICTLY_LESS = "Expected parameter 'P' to be strictly less than <%s> but was <%s>";

    static final String EXPECTED_GREATER = "Expected parameter to be greater than <%s> but was <%s>";
    static final String EXPECTED_P_GREATER = "Expected parameter 'P' to be greater than <%s> but was <%s>";
    static final String EXPECTED_STRICTLY_GREATER = "Expected parameter to be strictly greater than <%s> but was <%s>";
    static final String EXPECTED_P_STRICTLY_GREATER = "Expected parameter 'P' to be strictly greater than <%s> but was <%s>";

    /**
     * Utility class.
     */
    private NumberCheckersUtils() {
        super();
    }

}
