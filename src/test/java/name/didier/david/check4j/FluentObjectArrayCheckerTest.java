package name.didier.david.check4j;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import org.junit.jupiter.api.Test;

class FluentObjectArrayCheckerTest
        extends AbstractFluentCheckerTestBase<FluentObjectArrayChecker<Object>, Object[]> {

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isNotEmpty_should_pass_if_not_empty() {
        assertThatNoException().isThrownBy(
                () -> newChecker().isNotEmpty());
    }

    @Test
    void isNotEmpty_should_return_self() {
        FluentObjectArrayChecker<Object> checker = newChecker();
        assertThat(checker.isNotEmpty()).isSameAs(checker);
    }

    @Test
    void isNotEmpty_should_fail_if_null() {
        FluentObjectArrayChecker<Object> checker = newChecker(null);
        try {
            checker.isNotEmpty();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_MESSAGE);
        }
    }

    @Test
    void isNotEmpty_should_fail_if_null_with_pName() {
        FluentObjectArrayChecker<Object> checker = newChecker(null).as(P_NAME);
        try {
            checker.isNotEmpty();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_P_MESSAGE);
        }
    }

    @Test
    void isNotEmpty_should_fail_if_empty() {
        FluentObjectArrayChecker<Object> checker = newChecker(new Object[0]);
        try {
            checker.isNotEmpty();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to not be empty");
        }
    }

    @Test
    void isNotEmpty_should_fail_if_empty_with_pName() {
        FluentObjectArrayChecker<Object> checker = newChecker(new Object[0]).as(P_NAME);
        try {
            checker.isNotEmpty();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to not be empty");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected Object[] newActual() {
        return new Object[] { new Object() };
    }

    @Override
    protected FluentObjectArrayChecker<Object> newChecker() {
        return newChecker(newActual());
    }

    @Override
    protected FluentObjectArrayChecker<Object> newChecker(final Object[] actual) {
        return new FluentObjectArrayChecker<>(actual);
    }
}
