package name.didier.david.check4j;

class FluentIntegerCheckerTest
        extends AbstractFluentNumberCheckerTestBase<FluentIntegerChecker, Integer> {

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected FluentIntegerChecker newChecker(final Integer actual) {
        return new FluentIntegerChecker(actual);
    }

    @Override
    protected Integer numberN() {
        return -1;
    }

    @Override
    protected Integer numberZ() {
        return 0;
    }

    @Override
    protected Integer numberP() {
        return 1;
    }
}
