package name.didier.david.check4j;

import static name.didier.david.check4j.ConciseCheckers.checkExists;
import static name.didier.david.check4j.ConciseCheckers.checkIsFile;
import static name.didier.david.check4j.ConciseCheckers.checkIsDirectory;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import java.io.File;
import java.nio.file.Path;
import org.junit.jupiter.api.Test;

class ConcisePathCheckersTest
        extends AbstractConciseCheckersTestBase {

    private static final Path NULL = null;
    private static final Path NOT_EXISTING = new File("some/path/that/is/really/unlikely/to/exist").toPath();
    private static final Path EXISTING_DIRECTORY = new File("src/test/resources").toPath();
    private static final Path EXISTING_FILE = new File("src/test/resources/logback-test.xml").toPath();

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void checkExists_should_pass_if_path_exist() {
        assertThat(checkExists(EXISTING_DIRECTORY)).isEqualTo(EXISTING_DIRECTORY);
        assertThat(checkExists(EXISTING_DIRECTORY, P_NAME)).isEqualTo(EXISTING_DIRECTORY);

        assertThat(checkExists(EXISTING_FILE)).isEqualTo(EXISTING_FILE);
        assertThat(checkExists(EXISTING_FILE, P_NAME)).isEqualTo(EXISTING_FILE);
    }

    @Test
    void checkExists_should_fail_if_path_is_null() {
        try {
            checkExists(NULL);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_MESSAGE);
        }

        try {
            checkExists(NULL, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_P_MESSAGE);
        }
    }

    @Test
    void checkExists_should_fail_if_path_does_not_exist() {
        try {
            checkExists(NOT_EXISTING);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to be an existing path");
        }

        try {
            checkExists(NOT_EXISTING, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to be an existing path");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void checkIsFile_should_pass_if_path_is_a_regular_file() {
        assertThat(checkIsFile(EXISTING_FILE)).isEqualTo(EXISTING_FILE);
        assertThat(checkIsFile(EXISTING_FILE, P_NAME)).isEqualTo(EXISTING_FILE);
    }

    @Test
    void checkIsFile_should_fail_if_path_is_null() {
        try {
            checkIsFile(NULL);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_MESSAGE);
        }

        try {
            checkIsFile(NULL, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_P_MESSAGE);
        }
    }

    @Test
    void checkIsFile_should_fail_if_path_is_not_a_regular_file() {
        try {
            checkIsFile(EXISTING_DIRECTORY);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to be an existing regular file");
        }

        try {
            checkIsFile(EXISTING_DIRECTORY, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to be an existing regular file");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void checkIsFile_should_pass_if_path_is_a_regular_directory() {
        assertThat(checkIsDirectory(EXISTING_DIRECTORY)).isEqualTo(EXISTING_DIRECTORY);
        assertThat(checkIsDirectory(EXISTING_DIRECTORY, P_NAME)).isEqualTo(EXISTING_DIRECTORY);
    }

    @Test
    void checkIsDirectory_should_fail_if_path_is_null() {
        try {
            checkIsDirectory(NULL);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_MESSAGE);
        }

        try {
            checkIsDirectory(NULL, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_P_MESSAGE);
        }
    }

    @Test
    void checkIsFile_should_fail_if_path_is_not_a_regular_directory() {
        try {
            checkIsDirectory(EXISTING_FILE);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to be an existing regular directory");
        }

        try {
            checkIsDirectory(EXISTING_FILE, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to be an existing regular directory");
        }
    }

}
