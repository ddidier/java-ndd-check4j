package name.didier.david.check4j;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import org.junit.jupiter.api.Test;

abstract class AbstractFluentCharSequenceTestBase<C extends AbstractFluentCharSequenceChecker<C, A>, A extends CharSequence>
        extends AbstractFluentCheckerTestBase<C, A> {

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isNotEmpty_should_pass_if_not_empty() {
        assertThatNoException().isThrownBy(
                () -> newChecker().isNotEmpty());
    }

    @Test
    void isNotEmpty_should_pass_if_blank() {
        assertThatNoException().isThrownBy(
                () -> newChecker(blank()).isNotEmpty());
    }

    @Test
    void isNotEmpty_should_return_self() {
        AbstractFluentCharSequenceChecker<C, A> checker = newChecker();
        assertThat(checker.isNotEmpty()).isSameAs(checker);
    }

    @Test
    void isNotEmpty_should_fail_if_empty() {
        AbstractFluentCharSequenceChecker<C, A> checker = newChecker(empty());
        try {
            checker.isNotEmpty();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to not be empty");
        }
    }

    @Test
    void isNotEmpty_should_fail_if_empty_with_pName() {
        AbstractFluentCharSequenceChecker<C, A> checker = newChecker(empty()).as(P_NAME);
        try {
            checker.isNotEmpty();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to not be empty");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    protected abstract A blank();

    protected abstract A empty();

    @Override
    protected AbstractFluentCharSequenceChecker<C, A> newChecker() {
        return newChecker(newActual());
    }

    @Override
    protected abstract AbstractFluentCharSequenceChecker<C, A> newChecker(A actual);
}
