package name.didier.david.check4j;

abstract class AbstractConciseCheckersTestBase {

    protected static final String P_NAME = "P";

    protected static final String NULL_MESSAGE = "Expected parameter to not be null";
    protected static final String NULL_P_MESSAGE = "Expected parameter 'P' to not be null";

}
