/**
 * Check4J external API.
 * <p>
 * The main entry points are {@link name.didier.david.check4j.FluentCheckers} and
 * {@link name.didier.david.check4j.ConciseCheckers}.
 * </p>
 */
package name.didier.david.check4j;
