package name.didier.david.check4j;

/**
 * Checking methods for {@link Double}s. To create a new instance of this class, invoke
 * <code>{@link FluentCheckers#checkThat(Double)}</code>.
 */
public class FluentDoubleChecker
        extends AbstractFluentNumberChecker<FluentDoubleChecker, Double> {

    /**
     * Abstract constructor.
     *
     * @param actual the actual value to check.
     */
    protected FluentDoubleChecker(final Double actual) {
        super(actual, FluentDoubleChecker.class);
    }

    @Override
    protected Double zero() {
        return (double) 0;
    }
}
