package name.didier.david.check4j;

/**
 * Checking methods for {@link Integer}s. To create a new instance of this class, invoke
 * {@link FluentCheckers#checkThat(Integer)}.
 */
public class FluentIntegerChecker
        extends AbstractFluentNumberChecker<FluentIntegerChecker, Integer> {

    /**
     * Abstract constructor.
     *
     * @param actual the actual value to check.
     */
    protected FluentIntegerChecker(final Integer actual) {
        super(actual, FluentIntegerChecker.class);
    }

    @Override
    protected Integer zero() {
        return 0;
    }
}
