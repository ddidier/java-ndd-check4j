package name.didier.david.check4j;

import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Checking methods for {@link Path}s. To create a new instance of this class, invoke
 * <code>{@link FluentCheckers#checkThat(Path)}</code>.
 */
public class FluentPathChecker
        extends AbstractFluentChecker<FluentPathChecker, Path> {

    /**
     * Hidden constructor.
     *
     * @param actual the actual value to check.
     */
    protected FluentPathChecker(final Path actual) {
        super(actual, FluentPathChecker.class);
    }

    /**
     * Verifies that the actual path is not {@code null} and exist.
     *
     * @return {@code this} checker object.
     * @throws IllegalArgumentException if the actual path is {@code null} or does not exist.
     */
    public FluentPathChecker exists() {
        isNotNull();
        if (!Files.exists(actual)) {
            throw expectedParameterTo("be an existing path");
        }
        return myself;
    }

    /**
     * Verifies that the actual path is not {@code null} and is an existing regular file.
     *
     * @return {@code this} checker object.
     * @throws IllegalArgumentException if the actual path is {@code null} or is not an existing regular file.
     */
    public FluentPathChecker isFile() {
        isNotNull();
        if (!Files.isRegularFile(actual)) {
            throw expectedParameterTo("be an existing regular file");
        }
        return myself;
    }

    /**
     * Verifies that the actual path is not {@code null} and is an existing regular directory.
     *
     * @return {@code this} checker object.
     * @throws IllegalArgumentException if the actual path is {@code null} or is not an existing regular directory.
     */
    public FluentPathChecker isDirectory() {
        isNotNull();
        if (!Files.isDirectory(actual)) {
            throw expectedParameterTo("be an existing regular directory");
        }
        return myself;
    }
}
