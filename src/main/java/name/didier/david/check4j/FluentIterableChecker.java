package name.didier.david.check4j;

/**
 * Checking methods for array of {@link Iterable}s. To create a new instance of this class, invoke
 * <code>{@link FluentCheckers#checkThat(Iterable)}</code>.
 *
 * @param <I> the type of the "actual" iterable.
 * @param <T> the type of the "actual" value.
 */
public class FluentIterableChecker<I extends Iterable<T>, T>
        extends AbstractFluentChecker<FluentIterableChecker<I, T>, I> {

    /**
     * Hidden constructor.
     *
     * @param actual the actual value to check.
     */
    protected FluentIterableChecker(final I actual) {
        super(actual, FluentIterableChecker.class);
    }

    /**
     * Verifies that the actual value is not {@code null} nor empty.
     *
     * @return {@code this} checker object.
     * @throws IllegalArgumentException if the actual value is {@code null} or empty.
     */
    public FluentIterableChecker<I, T> isNotEmpty() {
        isNotNull();
        if (!actual.iterator().hasNext()) {
            throw expectedParameterTo("not be empty");
        }
        return myself;
    }
}
